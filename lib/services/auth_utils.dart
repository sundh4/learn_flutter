import 'package:shared_preferences/shared_preferences.dart';

class AuthUtils {

	static final String endPoint = '/api/accounts/login';

	// Keys to store and fetch data from SharedPreferences
	static final String authTokenKey = 'token';
	static final String userNameKey = 'userName';
	static final String fullNameKey = 'fullName';
	//static bool isSuccessKey = false;

	static String getToken(SharedPreferences prefs) {
		return prefs.getString(authTokenKey);
	}

	static insertDetails(SharedPreferences prefs, var response) {
		//prefs.setBool(isSuccessKey, response['isSuccess']);
		//prefs.setBool(isSuccessKey, true);
		var user = response['value'];
		prefs.setString(userNameKey, user['userName']);
		prefs.setString(fullNameKey, user['fullName']);
		prefs.setString(authTokenKey, user['token']);
	}
	
}