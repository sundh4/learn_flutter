import 'dart:async';

import 'package:my_first_app/app/pages/home_page.dart';
import 'package:my_first_app/services/auth_utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_first_app/services/network_utils.dart';
import 'package:my_first_app/app/components/error_box.dart';
import 'package:my_first_app/app/components/user_field.dart';
import 'package:my_first_app/app/components/password_field.dart';
import 'package:my_first_app/app/components/login_button.dart';
import 'package:my_first_app/app/components/register_button.dart';

class LoginPage extends StatefulWidget {

	@override
	LoginPageState createState() => new LoginPageState();

}

class LoginPageState extends State<LoginPage> {
	final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
	Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
	SharedPreferences _sharedPreferences;
	bool _isError = false;
	bool _obscureText = true;
	bool _isLoading = false;
	TextEditingController _userController, _passwordController;
	String _errorText, _userError, _passwordError;

	@override
	void initState() {
		super.initState();
		_fetchSessionAndNavigate();
		_userController = new TextEditingController();
		_passwordController = new TextEditingController();
	}

	_fetchSessionAndNavigate() async {
		_sharedPreferences = await _prefs;
		String authToken = AuthUtils.getToken(_sharedPreferences);
		if(authToken != null) {
			Navigator.of(_scaffoldKey.currentContext)
				.pushReplacementNamed(HomePage.routeName);
		}
	}

	_showLoading() {
		setState(() {
		  _isLoading = true;
		});
	}

	_hideLoading() {
		setState(() {
		  _isLoading = false;
		});
	}

	_authenticateUser() async {
		_showLoading();
		if(_valid()) {
			var responseJson = await NetworkUtils.authenticateUser(
					_userController.text, _passwordController.text
			);

			print(responseJson);

			if(responseJson == null) {
				NetworkUtils.showSnackBar(_scaffoldKey, 'Something went wrong!');
			} else if(responseJson == 'NetworkError') {
				NetworkUtils.showSnackBar(_scaffoldKey, null);
			} else if(responseJson['errorMessage'] == 'User not exist') {
				NetworkUtils.showSnackBar(_scaffoldKey, 'User not exist');
			} else if(responseJson['errorMessage'] == 'Incorrect Password') {
				NetworkUtils.showSnackBar(_scaffoldKey, 'Incorrect Password');
			} else {

				AuthUtils.insertDetails(_sharedPreferences, responseJson);
				/**
				 * Removes stack and start with the new page.
				 * In this case on press back on HomePage app will exit.
				 * **/
				Navigator.of(_scaffoldKey.currentContext)
					.pushReplacementNamed(HomePage.routeName);

			}
			_hideLoading();
		} else {
			setState(() {
				_isLoading = false;
			});
		}
	}

	_valid() {
		bool valid = true;

		if(_userController.text.isEmpty) {
			valid = false;
			_userError = "User or Email can't Empty!";
		}

		if(_passwordController.text.isEmpty) {
			valid = false;
			_passwordError = "Password can't Empty!";
		} /** else if(_passwordController.text.length < 6) {
			valid = false;
			_passwordError = "Password is invalid!";
		} **/

		return valid;
	}

	Widget _loginScreen() {
		return new Container(
			child: new ListView(
				padding: const EdgeInsets.only(
					top: 100.0,
					left: 16.0,
					right: 16.0
				),
				children: <Widget>[
					new ErrorBox(
						isError: _isError,
						errorText: _errorText
					),
					new UserField(
							userController: _userController,
							userError: _userError
					),
					new PasswordField(
							passwordController: _passwordController,
							obscureText: _obscureText,
							passwordError: _passwordError,
							togglePassword: _togglePassword,
					),
					new LoginButton(onPressed: _authenticateUser),
					new RegisterButton(onPressed: _authenticateUser)
				],
			),
		);
	}

	_togglePassword() {
		setState(() {
			_obscureText = !_obscureText;
		});
	}

	Widget _loadingScreen() {
		return new Container(
			margin: const EdgeInsets.only(top: 100.0),
			child: new Center(
				child: new Column(
					children: <Widget>[
						new CircularProgressIndicator(
							strokeWidth: 4.0
						),
						new Container(
							padding: const EdgeInsets.all(8.0),
							child: new Text(
								'Please Wait',
								style: new TextStyle(
									color: Colors.grey.shade500,
									fontSize: 16.0
								),
							),
						)
					],
				)
			)
		);
	}

	@override
	Widget build(BuildContext context) {
		return new Scaffold(
			key: _scaffoldKey,
			body: _isLoading ? _loadingScreen() : _loginScreen()
		);
	}

}