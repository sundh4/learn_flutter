import 'dart:async';
import 'package:my_first_app/services/auth_utils.dart';
import 'package:my_first_app/services/network_utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
	static final String routeName = 'home';

	@override
	State<StatefulWidget> createState() {
		return new _HomePageState();
	}

}

class _HomePageState extends State<HomePage> {
	GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
	Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

	SharedPreferences _sharedPreferences;
	var _authToken, _name, _namauser, _noKTP, _tglLahir, _gender, _agama,
			_gold, _addrss;

	@override
	void initState() {
		super.initState();
		_fetchSessionAndNavigate();
	}

	_fetchSessionAndNavigate() async {
		_sharedPreferences = await _prefs;
		String authToken = AuthUtils.getToken(_sharedPreferences);
		var name = _sharedPreferences.getString(AuthUtils.userNameKey);

		print(authToken);
		_fetchHome(authToken);
		setState(() {
			_authToken = authToken;
			_name = name;
		});

		if(_authToken == null) {
			_logout();
		}
	}

	_fetchHome(String authToken) async {
		var responseJson = await NetworkUtils.fetch(authToken, '/api/accounts/profile');

		if(responseJson == null) {
			NetworkUtils.showSnackBar(_scaffoldKey, 'Something went wrong!');
		} else if(responseJson == 'NetworkError') {
			NetworkUtils.showSnackBar(_scaffoldKey, null);
		} else if(responseJson['errors'] != null) {
			_logout();
		} else if(responseJson['isSuccess'] = false){
			NetworkUtils.showSnackBar(_scaffoldKey, 'Invalid Token');
		}

		setState(() {
			var user = responseJson['value'];
		  _namauser = user['nama'];
		  _noKTP = user['noKTP'];
			_tglLahir = DateFormat('dd-MM-yyyy').format(DateTime.parse(user['tanggalLahir']));
			if (["", null].contains(user["jenisKelamin"])) {
				_gender = 'Undefine';
			} else {
				_gender = user['jenisKelamin'];
			}
			_agama = user['agama'];
			_gold = user['golonganDarah'];
			if (["", null].contains(user["alamat"])) {
				_addrss = 'Undefine';
			} else {
				_addrss = user['alamat'];
			}
		});
	}

	_logout() {
		NetworkUtils.logoutUser(_scaffoldKey.currentContext, _sharedPreferences);
	}

	@override
	Widget build(BuildContext context) {
		return new Scaffold(
			key: _scaffoldKey,
			appBar: new AppBar(
				title: new Text('Home'),
			),
			body: new Container(
				margin: const EdgeInsets.symmetric(horizontal: 16.0),
				child: new Column(
					crossAxisAlignment: CrossAxisAlignment.stretch,
					children: <Widget>[
						new Container(
							padding: const EdgeInsets.all(8.0),
							child: new Text(
								"UserName: $_name \n"
										"Nama: $_namauser \n"
										"KTP: $_noKTP \n"
										"TanggalLahir: $_tglLahir \n"
										"Gender: $_gender \n"
										"Agama: $_agama \n"
										"Alamat: $_addrss \n"
										"Golongan Darah: $_gold",
								style: new TextStyle(
									fontSize: 24.0,
									color: Colors.grey.shade700
								),
							),
						),
						new MaterialButton(
							color: Theme.of(context).primaryColor,
							child: new Text(
								'Logout',
								style: new TextStyle(
									color: Colors.white
								),
							),
							onPressed: _logout
						),
					]
				),
			)
		);
	}

}